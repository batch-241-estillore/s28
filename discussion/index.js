//CRUD Operations
/*
	CRUD operations are the heart of any backend operations
	-CRUD stnads for Create, Read, Update, Delete
	-MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods
*/

// Create: Inserting documents
db.users //this is to create a collection?

//Insert one
/*
	Syntax:
		db.collectionName.insertOne({})

	Inserting/Assigning values in JS objects:
		object.object.method({object})
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234566",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","Javascript","Python"],
	department: "none"
});

//INSERT MANY
/*
db.users.insertMany([
	{objectA},
	{objectB}
])
*/

db.users.insertMany([
	{	
		firstName: "Stephen",
		lastName: "Hawking",
		age: "76",
		contact:{
			phone: "128937",
			email: "stephen@gmail.com"
		},
		courses: ["Python","React","PHP"],
		department: "none"
	},
	{	
		firstName: "Neil",
		lastName: "Armstrong",
		age: "82",
		contact:{
			phone: "2352346",
			email: "neil@gmail.com"
		},
		courses: ["React","Laravel","SASS"],
		department: "none"
	}
])

//Read: FINDING DOCUMENTS

//FIND ALL
/*
Syntax:
	db.collectionName.find();
*/
db.users.find();

//Finding users with single arguments
/*
	Syntax:
		db.collectionName.find({field: value})
*/
db.users.find({firstName: "Stephen"});


db.users.find({firstName: "Stephen", age: 20});
//no matching documents

db.users.find({firstName: "Stephen", age: 76});


// update: Updating documents
//Repeat Jane to be updated
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234566",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","Javascript","Python"],
	department: "none"
});

//UPDATE ONE
/*
Syntax:
	db.collectionName.updateOne({criteria},{$set: (field: value)})
*/

db.users.updateOne(
	{firstName: "Jane"},
{
	$set: {
		firstName: "Jane",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "000000",
			email: "janegates@gmail.com"
		},
		courses: ["AWS", "Google Cloud","Azure"],
		department: "infrastructure",
		status: "active"
	}
});

db.users.find({firstName: "Jane"});




//UPDATE MANY
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
)
db.users.find().pretty();


//REPLACE ONE
/*
	Syntax:
		db.collectionName.replaceOne({criteria},{$set: {field: value}})
*/

db.users.replaceOne(
	{lastName: "Gates"},
	{
		firstName: "Bill"
		lastName: "Clinton"
	}
)
db.users.find({firstName: "Bill"});

//DELETE: Deleting documents
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact:{
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})
db.users.find({firstName: "Test"});

//Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

db.users.deletOne({age: 76});
db.users.find();

//Delete many
db.users.deleteMany({
	courses: []
});

//Delete all
//db.users.delete()
/*
	-be careful when using the "delete" method if no search method is provided.
	-DO NOT USE: db.collectionName.deleteMany()
	Syntax:
		db.collectionName.deleteMany({criteria})
*/


//ADVANCED QUERIES

//Query an embedded document

db.users.find({
	contact: {
		phone: "1234566",
		email: "janedoe@gmail.com"
	}
})
//Find the document in nested fields
db.users.find({"contact.email": "janedoe@gmail.com"});

//Querying an array with exact elements

db.users.find({courses: ["React", "Laravel", "SASS"]});

//Querying an element without regard to order of the array
db.users.find({courses: {$all:["React", "SASS", "Laravel"]}});

//Make an array to query
db.users.insert({
	namearr: [
		{
			nameA: 'Juan'},
		{
			nameB: 'Tamad'
		}
	]
})

db.users.find({
	namearr: {
		nameA: "Juan"
	}
})











